package com.mseop.Servlet;

public class doGetdoPost {
	/*
		 서블릿이란?
		Server + Applet의 합성어, JAVA 언어를 이용하여 사용자의 요청을 받아
		처리하고 그 결과를 다시 사용자에게 전송하는 역할의 Class 파일을 말한다.
		즉 웹에서 동적인 페이지를 java로 구현한 서버측 프로그램이라고 보면 된다.
	
		 Servlet 은 HttpServlet라는 클래스를 상속 받는다.
		 Servlet은 java언어를 사용해 [  웹 프로그램  ] 을 제작 하는것, 
		 doGet doPost . error Code
		 
		 클라이언트요청들어옴 -> WAS에서는 request 객체와 response객체를 탐캣에서 생성해준다. 
		 ex) 로그인 기능 Id와 PW값을 request 객체에 담아서 전달한다.
		      그 이후 WAS에서 이를 받아 DAtaBase에서의 절차를 통해 검증하고 이해 대한 결과값을 reponse 객체에 실어서 클라이언트에게 전달한다.
		      
		      
		 doGet() & doPost()의 차이점 공부
		 Get : URL값으로 정보가 전송되어 보안에 취약,
		 Post : header를 이용해 정보가 전송되어 상대적으로 보안에 강하다.     
	 
	     
	     -------------------------------- doGet() --------------------------------
	     
	     form 태그
	     이 태그는 클라이언트의 데이터를 입력 받고, 이를 서버에 request하는 작업을 수행하기 위한 양식을 만들 때 주로 사용되는 태그

	     <form action="testmapper" method="get">
	     index.html 메소드에 get을 적어준다. 
	     action : 데이터를 전송할 URL & 매핑값
	     method : 데이터를 전달하는 방식 get & post
	     
	     get방식으로 입력받은 값을 servlet에서 request.getParameter메소드를 통해 가져와 콘솔창에 출력한다. 
	     * 가장 큰 특징은 URL에 변수를 포함시켜 요청한다는 점이다. 
	     * user_id(변수)=test(입력 값)&user_pw(변수)=1234(입력 값)  
	     
	      단점 : 로그인을 한다고 했을때 위와같이 주소창에 
			  	 아이디와 패스워드등이 그대로 노출이됨.
				 - 보안  취약 - 
				 
		  단점2 : 글자수에 제한이있어서 대량의데이터가 get방식으로
				 요청했을경우 데이터가 정상적으로 넘어가지않음
		
		
		 <input type = "submit" value = "전송"  전송버튼인 submit을 만듬. 이 버튼을 클릭하게되면 서블릿을 요청
		 	
	     </a> 태그를 사용하여 링크를 걸어주면 서블릿은 get방식
	     
	     [ 한글처리 ]
		 Servers/톰캣서버 폴더내에 있는 server.xml에서 <connector>에
		 URIEncoding="UTF-8"(EUC-KR, UTF-8 등 원하는 인코딩)을 추가
		 
		문자열을 이용해서 사용자에게 내보내기를 할 페이지를 작성한것, 
		StringBuilder responseBuilder = new StringBuilder();
		responseBuilder.append("<!doctype html>\r\n")
		                       .append("<hrml>\n")
		                       .append("<head>\n")
		                       .append("</head>\n")
		                       .append("<body>\n")
		                       .append("<h1>안녕 Servlet response</h1>\n")
		                       .append("</html>");
		 
		 
	     response.setContentType("text/html; charset=UTF-8");
	     
	     PrintWriter out = response.getWriter();
		//PrintWriter : 웹브라우저에 띄우기 위해 생긴 아이
	     
	     -------------------------------- doPost() --------------------------------
	     장점 - 처리용으로 많이 사용된다.
	     1. 전달 값이 Data Packet 부분에 넣어 전달되므로 노출이 되지 않아 보안성이 좋다.
		 2. Data 부분에 넣기 때문에 값의 크기에 제한이 없다.
		
		 [ 한글처리 ]
		 doPost메소드내에서 설정,
		  request.setCharacterEncoding("UTF-8");
		 

	     getParameter 한개 가져오기 getParameterValues 여러개 가져오기!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	     
	     
	     
	     
	     -------------------------------- 오류 코드 --------------------------------
	     404
	     1. 파일이 있는데 경로가 틀림.
	     2. 실제로 페이지가 없음
	     
	     405  get으로 했는데 받는걸 post로 했을시, 
	     HTTP 메소드 GET은 이 URL에 의해 지원이 되지않습니다..
	     
	     500
	     1. 개발자 실수
	     
	     
	     
	     
	     
	     
	     
	     
	     
	 
	  */

}
